---
title: "Merging Data"
author: "Signe Kirk Brødbæk and Sara Kolding"
date: "20 nov 2019"
output: html_document
---

```{r}
library(pacman)
p_load(tidyverse, rethinking, ggthemes, brms)
setwd("C:/Users/sarak/OneDrive - Aarhus universitet/COGNITIVE SCIENCE/FIFTH SEMESTER/BACHELOR")
```

## BioVid Meta Data

```{r}
#subjective ratings
Ratings <- read.csv("MovieRatings_86_Subjects.csv", sep = ";")
colnames(Ratings)[1] <- "ID"
Ratings$movieAmusement <- ifelse(Ratings$movieAmusement == 1, "Amusement1", ifelse(Ratings$movieAmusement == 2, "Amusement2", "Amusement3"))
Ratings$movieAnger <- ifelse(Ratings$movieAnger == 1, "Anger1", ifelse(Ratings$movieAnger == 2, "Anger2", "Anger3"))
Ratings$movieDisgust <- ifelse(Ratings$movieDisgust == 1, "Disgust1", ifelse(Ratings$movieDisgust == 2, "Disgust2", "Disgust3"))
Ratings$movieFear <- ifelse(Ratings$movieFear == 1, "Fear1", ifelse(Ratings$movieFear == 2, "Fear2", "Fear3"))
Ratings$movieSadness <- ifelse(Ratings$movieSadness == 1, "Sadness2", ifelse(Ratings$movieSadness == 2, "Sadness2", "Sadness3"))

#Long format
ratingsValence <- Ratings[,c(1:4, 6, 9, 12, 15, 18)]
ratingsArousal <- Ratings[,c(1:4, 7, 10, 13, 16, 19)]
ratingsClip <- Ratings[,c(1:4, 5, 8, 11, 14, 17)]

ratingsValenceLong <- gather(ratingsValence, Emotion, Valence, c(5:9))
ratingsArousalLong <- gather(ratingsArousal, Emotion, Arousal, c(5:9))
ratingsClipLong <- gather(ratingsClip, Emotion, Clip, c(5:9))

ratingsValenceLong$Emotion <- str_match(ratingsValenceLong$Emotion, "Amusement|Anger|Sadness|Disgust|Fear")
ratingsArousalLong$Emotion <- str_match(ratingsArousalLong$Emotion, "Amusement|Anger|Sadness|Disgust|Fear")
ratingsClipLong$Emotion <- str_match(ratingsClipLong$Emotion, "Amusement|Anger|Sadness|Disgust|Fear")

Ratings <- full_join(ratingsArousalLong, ratingsValenceLong)
Ratings <- full_join(Ratings, ratingsClipLong)


#merging
#Changing ID
Ratings$ID <- str_match(Ratings$ID,"(\\d+[-]\\d+)")

write.csv(Ratings, "BioVidMetaData.csv")
```

## Mahnob Meta Data

```{r}
#Load session
setwd("C:/Users/sarak/OneDrive - Aarhus universitet/COGNITIVE SCIENCE/FIFTH SEMESTER/BACHELOR/Mahnob-HCI Tagging Database")

load_session <- function(session) {
  #read xml file as data frame
  d <- read_xml(paste0("CompleteData/Sessions/", session, "/session.xml"))
  d1 <- as.data.frame(xml_attrs(d))
  d <- as.data.frame(t(d1))
  #read nodes
  n <- read_html(paste0("CompleteData/Sessions/", session, "/session.xml"))
  n1 <- html_nodes(n, "subject")
  n2 <- as.data.frame(xml_attrs(n1))
  n <- as.data.frame(t(n2))
  #select variables
  vars1 <- d[,c(1, 2, 7, 8, 19)]
  names(vars1) = c("Session","Date","Arousal", "Valence", "Clip")
  vars2 <- as.data.frame(n[,1])
  names(vars2) = "ID"
  vars <- merge(vars1, vars2)
  return(vars)
}

Sessions = list.files(path = "CompleteData/Sessions/") %>%
    purrr::map_df(load_session)

#Load subjects
read_subjects <- function(subject) {
  #read xml file as data frame
  d <- read_xml(paste0("CompleteData/Subjects/", subject))
  d1 <- as.data.frame(xml_attrs(d))
  d <- as.data.frame(t(d1))
  #select variables
  vars <- d[c("id", "dob", "gender")]
  names(vars) = c("ID","DOB","Gender")
  #combine all this data
  return(vars)
}

Subjects = list.files(path = "CompleteData/Subjects/",pattern = ".xml") %>%
    purrr::map_df(read_subjects)

MetaData <- full_join(Sessions, Subjects)
MetaData <- MetaData[-451,]

MetaData$Age <- age_calc(as.Date(MetaData$DOB), enddate = as.Date(MetaData$Date), units = "years")

write.csv(MetaData, "MahnobMetaData.csv")
```

## Merging Data

```{r}
BioVidDat <- read.csv("BioVidMetaData.csv")
BioVidDat <- BioVidDat[,c(2, 5:10)]
colnames(BioVidDat)[1] <- "ID"

MahnobDat <- read.csv("MahnobMetaData.csv")
MahnobDat <- MahnobDat[,-1]

BioVidHR <- read.csv("hrfeatures_w_centered_BioVid.csv")
BioVidRP <- read.csv("rpfeatures_scaled_BioVid.csv")
BioVidECG <- full_join(BioVidHR, BioVidRP)
BioVidECG <- BioVidECG[,-c(1, 2)]

MahnobHR <- read.csv("hrfeatures_w_centered_Mahnob.csv")
MahnobRP <- read.csv("rpfeatures_scaled_Mahnob.csv")
MahnobECG <- full_join(MahnobHR, MahnobRP)
MahnobECG <- MahnobECG[,-c(1, 2)]

colnames(MahnobECG)[3] <- "Clip"
BioVidECG$Emotion <- as.character(BioVidECG$Emotion)
BioVidECG$Emotion[BioVidECG$Emotion == "amu"] <- "Amusement"
BioVidECG$Emotion[BioVidECG$Emotion == "ang"] <- "Anger"
BioVidECG$Emotion[BioVidECG$Emotion == "sad"] <- "Sadness"
BioVidECG$Emotion[BioVidECG$Emotion == "dis"] <- "Disgust"
BioVidECG$Emotion[BioVidECG$Emotion == "fea"] <- "Fear"
BioVid <- full_join(BioVidDat, BioVidECG)

write.csv(BioVid, "BioVidECG.csv")

MahnobDat$Clip <- str_replace_all(MahnobDat$Clip, ".avi", "")
Mahnob <- full_join(MahnobDat, MahnobECG)
write.csv(Mahnob, "MahnobECG.csv")

Mahnob$ID <- as.factor(as.character(Mahnob$ID))
Mahnob <- Mahnob[,-c(1, 2, 7)]
colnames(BioVid)[2] <- "Gender"
BioVid$Study <- "BioVid"
Mahnob$Study <- "Mahnob"
BioVid <- BioVid[, -4]

Data <- full_join(BioVid, Mahnob)
write.csv(Data, "FullData.csv")
```

