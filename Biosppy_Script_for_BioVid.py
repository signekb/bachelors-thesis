import numpy as np
from biosppy.signals import ecg
from biosppy.signals import tools
from matplotlib import pyplot as plt
from biosppy import plotting, utils
import matplotlib.image as mpimg
import os

files = os.listdir(".")

for f in files:
    signal = np.loadtxt(f)
    out = ecg.ecg(signal=signal, sampling_rate=256., show=False)
    plotting.plot_ecg(ts=out['ts'],
                    raw=signal,
                    filtered=out['filtered'],
                    rpeaks=out['rpeaks'],
                    templates_ts=out['templates_ts'],
                    templates=out['templates'],
                    heart_rate_ts=out['heart_rate_ts'],
                    heart_rate=out['heart_rate'],
                    path=f"./BioPlots/{f}.png",
                    show=False)

for f in files:
    signal = np.loadtxt(f)
    out = ecg.ecg(signal=signal, sampling_rate=256., show=False)
    np.savetxt(fname=f"TShr/{f}_hr_ts.txt", X=out['heart_rate_ts'], fmt="%s")

