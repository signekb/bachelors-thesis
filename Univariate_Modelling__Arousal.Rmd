---
title: 'Univariate Modelling: Arousal'
author: "Signe Kirk Brødbæk and Sara Kolding"
date: "25 nov 2019"
output: html_document
---

```{r}
library(pacman)
p_load(tidyverse, rethinking, ggthemes, brms, beepr, bayesplot, rstanarm, extrafont, gridExtra)
setwd("C:/Users/sarak/OneDrive - Aarhus universitet/COGNITIVE SCIENCE/FIFTH SEMESTER/BACHELOR")
Data <- read.csv("FullData.csv")
```

## Mean Heart Rate

```{r HRsMean}
#Prior predictive check
HRsMean <- bf(HRsMean ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsMean, Data)

prior = c(prior(normal(0, 1), class = Intercept),
                prior(normal(0, 1), class = b, coef = "StudyMahnob"),
                prior(normal(0, 1), class = b, coef = "moArousal"),
                prior(normal(0, .5), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .1), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, 1), class = sigma))

HRsMeanAPrior <- brm(HRsMean, 
                Data, family = gaussian,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains = 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsMeanAPrior, nsamples = 100)
print(HRsMeanAPrior)
plot(HRsMeanAPrior)

#Modelling
HRsMeanA <- 
  brm(data = Data, family = gaussian,
      HRsMean,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsMeanA, nsamples = 100) #posterior predictive check
print(HRsMeanA)
plot(HRsMeanA) #trace plot

save(HRsMeanA, file = "HRsMeanA.rda", compress = "xz")
```

## Minimum Heart Rate

```{r HRsMin}
#Prior predictive check
HRsMin <- bf(HRsMin ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsMin, Data)

prior = c(prior(normal(0, 1), class = Intercept),
                prior(normal(0, 1), class = b, coef = "StudyMahnob"),
                prior(normal(0, 1), class = b, coef = "moArousal"),
                prior(normal(0, .5), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .1), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, 1), class = sigma))

#Modelling
HRsMinAPrior <- brm(HRsMin, 
                Data, family = gaussian,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains = 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(sound=4)

print(HRsMinAPrior)
plot(HRsMinAPrior)
pp_check(HRsMinAPrior, nsamples = 100)

#Modelling
HRsMinA <- 
  brm(data = Data, family = gaussian,
      HRsMin,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(HRsMinA)
plot(HRsMinA) #trace plot
pp_check(HRsMinA, nsamples = 100) #posterior predictive check

save(HRsMinA, file = "HRsMinA.rda", compress = "xz")
```

## Maximum Heart Rate

```{r HRsMax}
#Prior predictive check
HRsMax <- bf(HRsMax ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsMax, Data)

prior = c(prior(normal(0, 1), class = Intercept),
                prior(normal(0, 1), class = b, coef = "StudyMahnob"),
                prior(normal(0, 1), class = b, coef = "moArousal"),
                prior(normal(0, .5), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .1), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, 1), class = sigma))

HRsMaxAPrior <- brm(HRsMax, 
                Data, family = gaussian,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains = 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(sound=4)

print(HRsMaxAPrior)
plot(HRsMaxAPrior)
pp_check(HRsMaxAPrior, nsamples = 100)

#Modelling
HRsMaxA <- 
  brm(data = Data, family = gaussian,
      HRsMax,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(HRsMaxA)
plot(HRsMaxA) #trace plot
pp_check(HRsMaxA, nsamples = 100) #posterior predictive check

save(HRsMaxA, file = "HRsMaxA.rda", compress = "xz")
```

## Heart Rate Range

```{r HRsRange}
#Prior predictive check
HRsRange <- bf(HRsRange ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsRange, Data)

prior = c(prior(normal(0, .5), class = Intercept),
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))


HRsRangeAPrior <- brm(HRsRange, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains = 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsRangeAPrior, nsamples = 100)
print(HRsRangeAPrior)
plot(HRsRangeAPrior)

#Modelling
HRsRangeA <- 
  brm(data = Data, family = lognormal,
      HRsRange,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsRangeA, nsamples = 100) #posterior predictive check
print(HRsRangeA)
plot(HRsRangeA) #trace plot

save(HRsRangeA, file = "HRsRangeA.rda", compress = "xz")
```

## Median Heart Rate

```{r HRsMedian}
#Prior predictive check
HRsMedian <- bf(HRsMedian ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsMedian, Data)

prior = c(prior(normal(0, 1), class = Intercept),
                prior(normal(0, 1), class = b, coef = "StudyMahnob"),
                prior(normal(0, 1), class = b, coef = "moArousal"),
                prior(normal(0, .5), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .1), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, 1), class = sigma))

HRsMedianAPrior <- brm(HRsMedian, 
                Data, family = gaussian,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains = 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsMedianAPrior, nsamples = 100)
print(HRsMedianAPrior)
plot(HRsMedianAPrior)

#Modelling
HRsMedianA <- 
  brm(data = Data, family = gaussian,
      HRsMedian,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsMedianA, nsamples = 100) #posterior predictive check
print(HRsMedianA)
plot(HRsMedianA) #trace plot

save(HRsMedianA, file = "HRsMedianA.rda", compress = "xz")
```

## Interquartile Range

```{r HRsIQR}
#Prior predictive check
HRsIQR <- bf(HRsIQR ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsIQR, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

HRsIQRAPrior <- brm(HRsIQR, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

pp_check(HRsIQRAPrior, nsamples = 100)
print(HRsIQRAPrior)
plot(HRsIQRAPrior)

#Modelling
HRsIQRA <- 
  brm(data = Data, family = lognormal,
      HRsIQR,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsIQRA, nsamples = 100) #posterior predictive check
print(HRsIQRA)
plot(HRsIQRA) #trace plot

save(HRsIQRA, file = "HRsIQRA.rda", compress = "xz")
```

## Mean Absolute Deviation

```{r HRsMAD}
#Prior predictive check
HRsMAD <- bf(HRsMAD ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsMAD, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

HRsMADAPrior <- brm(HRsMAD, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

pp_check(HRsMADAPrior, nsamples = 100)
print(HRMADAPrior)
plot(HRsMADAPrior)

#Modelling
HRsMADA <- 
  brm(data = Data, family = lognormal,
      HRsMAD,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

pp_check(HRsMADA, nsamples = 100) #posterior predictive check
print(HRsMADA)
plot(HRsMADA) #trace plot

save(HRsMADA, file = "HRsMADA.rda", compress = "xz")
```

## Heart Rate Acceleration

```{r HRsAcc}
#Prior predictive check
HRsAcc <- bf(HRsAcc ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRsAcc, Data)

prior = c(prior(normal(0, 1), class = Intercept),
                prior(normal(0, 1), class = b, coef = "StudyMahnob"),
                prior(normal(0, 1), class = b, coef = "moArousal"),
                prior(normal(0, .5), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .1), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, 1), class = sigma))

HRsAccAPrior <- brm(HRsAcc, 
                Data, family = gaussian,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains = 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(sound=4)

print(HRsAccAPrior)
pp_check(HRsAccAPrior, nsamples = 100)
plot(HRsAccAPrior)

#Modelling
HRsAccA <- 
  brm(data = Data, family = gaussian,
      HRsAcc,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(HRsAccA)
pp_check(HRsAccA, nsamples = 100) #posterior predictive check
plot(HRsAccA) #trace plot

save(HRsAccA, file = "HRsAccA.rda", compress = "xz")
```

## SDNN

```{r SDNN}
#Prior predictive check
SDNNs <- bf(SDNNs ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(SDNNs, Data)

prior = c(prior(normal(0, .5), class = Intercept),
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

SDNNsAPrior <- brm(SDNNs, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(SDNNsAPrior)
pp_check(SDNNsAPrior, nsamples = 100)
plot(SDNNsAPrior)

#Modelling
SDNNsA <- 
  brm(data = Data, family = lognormal,
      SDNNs,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(SDNNsA)
pp_check(SDNNsA, nsamples = 100) #posterior predictive check
plot(SDNNsA) #trace plot

save(SDNNsA, file = "SDNNsA.rda", compress = "xz")
```

## pNN50

```{r pNN50}
#Removing all NAs
Data$pNN50r <- Data$pNN50s
Data$pNN50r[Data$pNN50r == 0] <- NA

#Prior predictive check
pNN50r <- bf(pNN50r ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(pNN50r, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

pNN50rAPrior <- brm(pNN50r, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(pNN50rAPrior)
pp_check(pNN50rAPrior, nsamples = 100)
plot(pNN50rAPrior)

#Modelling
pNN50rA <- 
  brm(data = Data, family = lognormal,
      pNN50r,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(pNN50rA)
pp_check(pNN50rA, nsamples = 100) #posterior predictive check
plot(pNN50rA) #trace plot

save(pNN50rA, file = "pNN50rA.rda", compress = "xz")
```

## SDSD

```{r SDSD}
#Prior predictive check
SDSDs <- bf(SDSDs ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(SDSDsm, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

SDSDsAPrior <- brm(SDSDs, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(SDSDsAPrior)
pp_check(SDSDsAPrior, nsamples = 100)
plot(SDSDsAPrior)

#Modelling
SDSDsA <- 
  brm(data = Data, family = lognormal,
      SDSDs,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(SDSDsA)
pp_check(SDSDsA, nsamples = 100) #posterior predictive check
plot(SDSDsA) #trace plot

save(SDSDsA, file = "SDSDsA.rda", compress = "xz")
```

## rMSSD

```{r rMSSD}
#Prior predictive check
rMSSDs <- bf(rMSSDs ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(rMSSDs, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

rMSSDsAPrior <- brm(rMSSDs, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(rMSSDsAPrior)
pp_check(rMSSDsAPrior, nsamples = 100)
plot(rMSSDsAPrior)

#Modelling
rMSSDsA <- 
  brm(data = Data, family = lognormal,
      rMSSDs,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(rMSSDsA)
pp_check(rMSSDsA, nsamples = 100) #posterior predictive check
plot(rMSSDsA) #trace plot

save(rMSSDsA, file = "rMSSDsA.rda", compress = "xz")
```

## IRRR

```{r IRRR}
#Prior predictive check
IRRRs <- bf(IRRRs ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(IRRRs, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

IRRRsAPrior <- brm(IRRRs, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(IRRRsAPrior)
pp_check(IRRRsAPrior, nsamples = 100)
plot(IRRRsAPrior)

#Modelling
IRRRsA <- 
  brm(data = Data, family = lognormal,
      IRRRs,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(IRRRsA)
pp_check(IRRRsA, nsamples = 100) #posterior predictive check
plot(IRRRsA) #trace plot

save(IRRRsA, file = "IRRRsA.rda", compress = "xz")
```

## TINN

```{r TINN}
#Prior predictive check
TINNs <- bf(TINNs ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(TINNs, Data)

prior = c(prior(normal(0, .5), class = Intercept),
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

TINNsAPrior <- brm(TINNs, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(TINNsAPrior)
pp_check(TINNsAPrior, nsamples = 100)
plot(TINNsAPrior)

#Modelling
TINNsA <- 
  brm(data = Data, family = lognormal,
      TINNs,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(TINNsA)
pp_check(TINNsA, nsamples = 100) #posterior predictive check
plot(TINNsA) #trace plot

save(TINNsA, file = "TINNsA.rda", compress = "xz")
```

## HRV Index

```{r HRVis}
#Prior predictive check
HRVis <- bf(HRVis ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(HRVis, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

HRVisAPrior <- brm(HRVis, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(HRVisAPrior)
pp_check(HRVisAPrior, nsamples = 100)
plot(HRVisAPrior)

#Modelling
HRVisA <- 
  brm(data = Data, family = lognormal,
      HRVis,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(HRVisA)
pp_check(HRVisA, nsamples = 100) #posterior predictive check
plot(HRVisA) #trace plot

save(HRVisA, file = "HRVisA.rda", compress = "xz")
```

## MADRR

```{r MADRRs}
#Prior predictive check
MADRRs <- bf(MADRRs ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(MADRRs, Data)

prior = c(prior(normal(0, .5), class = Intercept), 
                prior(normal(0, .1), class = b, coef = "StudyMahnob"),
                prior(normal(0, .1), class = b, coef = "moArousal"),
                prior(normal(0, .1), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .05), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, .1), class = sigma))

MADRRsAPrior <- brm(MADRRs, 
                Data, family = lognormal,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains= 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(4)

print(MADRRsAPrior)
pp_check(MADRRsAPrior, nsamples = 100)
plot(MADRRsAPrior)

#Modelling
MADRRsA <- 
  brm(data = Data, family = lognormal,
      MADRRs,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(MADRRsA)
pp_check(MADRRsA, nsamples = 100) #posterior predictive check
plot(MADRRsA) #trace plot

save(MADRRsA, file = "MADRRsA.rda", compress = "xz")
```

## Interbeat Interval

```{r}
#Prior predictive check
MeanNNs <- bf(MeanNNs ~ 1 + Study + mo(Arousal) + Study:mo(Arousal) + 
                  (1 + mo(Arousal)|ID) + 
                  (1 + mo(Arousal)|Clip))

get_prior(MeanNNs, Data)

prior = c(prior(normal(0, 1), class = Intercept), #overall mean is 0, because it is centered
                prior(normal(0, 1), class = b, coef = "StudyMahnob"),
                prior(normal(0, 1), class = b, coef = "moArousal"),
                prior(normal(0, .5), class = b, coef = "moArousal:StudyMahnob"),
                prior(normal(0, .1), class = "sd"),
                prior(lkj(5), class = "cor"),
                prior(normal(0, 1), class = sigma))

MeanNNsAPrior <- brm(MeanNNs, 
                Data, family = gaussian,
                prior = prior, 
                sample_prior = "only", 
                iter = 2000, 
                warmup = 1000,
                chains = 2,
                cores = 4,
                control = list(adapt_delta = 0.99))
beep(sound=4)

print(MeanNNsAPrior)
pp_check(MeanNNsAPrior, nsamples = 100)
plot(MeanNNsAPrior)

#Modelling
MeanNNsA <- 
  brm(data = Data, family = gaussian,
      MeanNNs,
      prior = prior,
      iter = 8000, warmup = 4000, chains = 4, cores = 4,
      seed = 4, control = list(adapt_delta = 0.99))
beep(sound=4)

print(MeanNNsA)
pp_check(MeanNNsA, nsamples = 100) #posterior predictive check
plot(MeanNNsA) #trace plot

save(MeanNNsA, file = "MeanNNsA.rda", compress = "xz")
```
