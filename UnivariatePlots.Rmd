---
title: "Univariate plots"
author: "Signe Kirk Brødbæk and Sara Kolding"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(brms); library(tidyverse); library(ggdistribute)

setwd("~/Dropbox/AU/5_semester/Ba/Analysis/Models")

ArousalData <- read.csv("Arousal.csv")
ValenceData <- read.csv("Valence.csv")
```

# Load arousal models
```{r}
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsAccA.rda")
pAccA <- as.matrix(HRsAccA)
pAccA <- as.data.frame(pAccA)
pAccA$model <- "Acceleration"
pAccA <- pAccA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsIQRA.rda")
pIQRA <- as.matrix(HRsIQRA)
pIQRA <- as.data.frame(pIQRA)
pIQRA$model <- "Interquartile Range"
pIQRA <- pIQRA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsMADA.rda")
pMADA <- as.matrix(HRsMADA)
pMADA <- as.data.frame(pMADA)
pMADA$model <- "Mean Absolute Deviation"
pMADA <- pMADA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsMaxA.rda")
pMaxA <- as.matrix(HRsMax)
pMaxA <- as.data.frame(pMaxA)
pMaxA$model <- "Maximum Value"
pMaxA <- pMaxA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsMeanA.rda")
pMeanA <- as.matrix(HRsMeanA)
pMeanA <- as.data.frame(pMeanA)
pMeanA$model <- "Mean"
pMeanA <- pMeanA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsMedianA.rda")
pMedianA <- as.matrix(HRsMedian)
pMedianA <- as.data.frame(pMedianA)
pMedianA$model <- "Median"
pMedianA <- pMedianA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsMinA.rda")
pMinA <- as.matrix(HRsMin)
pMinA <- as.data.frame(pMinA)
pMinA$model <- "Minimum Value"
pMinA <- pMinA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsRangeA.rda")
pRangeA <- as.matrix(HRsRangeA)
pRangeA <- as.data.frame(pRangeA)
pRangeA$model <- "Range"
pRangeA <- pRangeA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRVisA.rda")
pHRViA <- as.matrix(HRVisA)
pHRViA <- as.data.frame(pHRViA)
pHRViA$model <- "HRV index"
pHRViA <- pHRViA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/IRRRsA.rda")
pIRRRsA <- as.matrix(IRRRsA)
pIRRRsA <- as.data.frame(pIRRRsA)
pIRRRsA$model <- "IRRR"
pIRRRsA <- pIRRRsA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/MADRRsA.rda")
pMADRRsA <- as.matrix(MADRRsA)
pMADRRsA <- as.data.frame(pMADRRsA)
pMADRRsA$model <- "MAD-RR"
pMADRRsA <- pMADRRsA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/MeanNNsA.rda")
pMeanNNsA <- as.matrix(MeanNNsA)
pMeanNNsA <- as.data.frame(pMeanNNsA)
pMeanNNsA$model <- "Inter-beat Interval"
pMeanNNsA <- pMeanNNsA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/pNN50rA.rda")
ppNN50rA <- as.matrix(pNN50rA)
ppNN50rA <- as.data.frame(ppNN50rA)
ppNN50rA$model <- "pNN50"
ppNN50rA <- ppNN50rA[,c(3,4,293)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/rMSSDsA.rda")
prMSSDsA <- as.matrix(rMSSDsA)
prMSSDsA <- as.data.frame(prMSSDsA)
prMSSDsA$model <- "r-MSSD"
prMSSDsA <- prMSSDsA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/SDNNsA.rda")
pSDNNsA <- as.matrix(SDNNsA)
pSDNNsA <- as.data.frame(pSDNNsA)
pSDNNsA$model <- "SDNN"
pSDNNsA <- pSDNNsA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/SDSDsA.rda")
pSDSDsA <- as.matrix(SDSDsA)
pSDSDsA <- as.data.frame(pSDSDsA)
pSDSDsA$model <- "SDSD"
pSDSDsA <- pSDSDsA[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/TINNsA.rda")
pTINNsA <- as.matrix(TINNsA)
pTINNsA <- as.data.frame(pTINNsA)
pTINNsA$model <- "TINN"
pTINNsA <- pTINNsA[,c(3,4,315)]

Arousal <- full_join(pAccA,pIQRA)
Arousal <- full_join(Arousal,pMADA)
Arousal <- full_join(Arousal,pMaxA)
Arousal <- full_join(Arousal,pMeanA)
Arousal <- full_join(Arousal,pMedianA)
Arousal <- full_join(Arousal,pRangeA)
Arousal <- full_join(Arousal,pMinA)
Arousal <- full_join(Arousal,pHRViA)
Arousal <- full_join(Arousal,pIRRRsA)
Arousal <- full_join(Arousal,pMADRRsA)
Arousal <- full_join(Arousal,pMeanNNsA)
Arousal <- full_join(Arousal,ppNN50rA)
Arousal <- full_join(Arousal,prMSSDsA)
Arousal <- full_join(Arousal,pSDNNsA)
Arousal <- full_join(Arousal,pSDSDsA)
Arousal <- full_join(Arousal,pTINNsA)
colnames(Arousal) <- c("Arousal","Interaction","Model")

write.csv(Arousal,"Arousal.csv")
                    


HR <- c("Acceleration","Interquartile Range","Maximum Value","Mean","Mean Absolute Deviation","Median","Minimum Value","Range")
HR <- as.data.frame(HR)
ArousalData$FeatureGroup <- se(ArousalData$Model %in% HR$HR,"HR","HRV")

ArousalData$Model <- factor(ArousalData$Model,levels = c("TINN","SDSD","SDNN","r-MSSD", "pNN50", "MAD-RR", "IRRR", "Inter-beat Interval", "HRV index","Range","Minimum Value","Median","Mean Absolute Deviation","Mean","Maximum Value","Interquartile Range","Acceleration"))
  
write.csv(ArousalData,"Arousal.csv")
```

# Univariate Arousal Plot
```{r}
ArousalData <- read.csv("Arousal.csv")

levels(ArousalData$Model)[levels(ArousalData$Model)=="Maximum Value"] <- "Maximum"
levels(ArousalData$Model)[levels(ArousalData$Model)== 'Minimum Value'] <- 'Minimum'
levels(ArousalData$Model)[levels(ArousalData$Model) == 'Interquartile Range'] <- 'IQR'
levels(ArousalData$Model)[levels(ArousalData$Model) == 'Mean Absolute Deviation'] <- 'MAD'
levels(ArousalData$Model)[levels(ArousalData$Model) == 'HRV index'] <- 'HRV Index'
levels(ArousalData$Model)[levels(ArousalData$Model) == 'Inter-beat Interval'] <- 'IBI'

ArousalData$Model <- factor(ArousalData$Model,levels = c("TINN","SDSD","SDNN","r-MSSD", "pNN50", "MAD-RR", "IRRR", "IBI", "HRV Index","Range","Minimum","Median","MAD","Mean","Maximum","IQR","Acceleration"))


arousalPlot <- ggplot(ArousalData, aes(x = Arousal, y = Model)) +
  geom_posterior(aes_string(x="Arousal"),draw_sd=FALSE,draw_ci = TRUE,brighten=c(10, 1, 3),ci_width=0.95) +
  ggtitle("Arousal Coefficient") + 
  xlab("") + 
  ylab(" ") + 
  geom_vline(alpha=0.5, color="black", size=0.333, linetype=1, xintercept=0) +
  facet_grid("FeatureGroup ~ .", scales="free_y", space="free_y") +
    theme_minimal() +
   theme(
     legend.position = "none",
     #panel.spacing=unit(.05, "lines"),
     panel.border=element_rect(fill=NA, color="black", size=0.67),
     text=element_text(family="Times New Roman"), 
     strip.background = element_rect(color = "black", size = 0.67),
     axis.text=element_text(size=12),
     plot.title=element_text(size=16,hjust=0.5)
  )

arousalInterPlot <- ggplot(ArousalData, aes(x = Interaction, y = Model)) +
  geom_posterior(aes_string(x="Interaction"),draw_sd=FALSE,draw_ci = TRUE,brighten=c(10, 1, 3),ci_width=0.95) +
  ggtitle("Arousal and Study") + 
  xlab("") + 
  ylab(" ") + 
  geom_vline(alpha=0.5, color="black", size=0.333, linetype=1, xintercept=0) +
  facet_grid("FeatureGroup ~ .", scales="free_y", space="free_y") +
    theme_minimal() +
   theme(
     legend.position = "none",
     #panel.spacing=unit(.05, "lines"),
     panel.border=element_rect(fill=NA, color="black", size=0.67),
     text=element_text(family="Times New Roman"), 
     strip.background = element_rect(color = "black", size = 0.67),
     axis.text=element_text(size=12),
     plot.title=element_text(size=16,hjust=0.5)
  )

## Close up at range
ArousalRange <- filter(ArousalData, Model == "Range")

arousalPlotRange <- ggplot(ArousalRange, aes(x = Arousal, y = Model)) +
  stat_density_ci(center_stat = "mean", ci_width = 0.95,interval_type = "ci",) +
  geom_posterior(aes_string(x="Arousal"),draw_sd=FALSE,draw_ci = TRUE,brighten=c(10, 1, 3)) + 
                 #fill="#587375",midline = "darkgrey") +
  ggtitle("Arousal Coefficient: Univariate, Range") + 
  xlab("") + 
  ylab(" ") + 
  geom_vline(alpha=0.5, color="black", size=0.333, linetype=1, xintercept=0) +
  #facet_grid("FeatureGroup ~ .", scales="free_y", space="free_y") +
   theme_minimal() +
   theme(
     legend.position = "none",
     #panel.spacing=unit(.05, "lines"),
     panel.border=element_rect(fill=NA, color="black", size=0.67),
     text=element_text(family="Times New Roman"), 
     strip.background = element_rect(color = "black", size = 0.67),
     axis.text=element_text(size=12),
     plot.title=element_text(size=16,hjust=0.5)
  )

```

# Load valence models
```{r}
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsAccV.rda")
pAccV <- as.matrix(HRsAccV)
pAccV <- as.data.frame(pAccV)
pAccV$model <- "Acceleration"
pAccV <- pAccV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsIQRV.rda")
pIQRV <- as.matrix(HRsIQRV)
pIQRV <- as.data.frame(pIQRV)
pIQRV$model <- "Interquartile Range"
pIQRV <- pIQRV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsMADV.rda")
pMADV <- as.matrix(HRsMADV)
pMADV <- as.data.frame(pMADV)
pMADV$model <- "Mean Absolute Deviation"
pMADV <- pMADV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsMaxV.rda")
pMaxV <- as.matrix(HRsMaxV)
pMaxV <- as.data.frame(pMaxV)
pMaxV$model <- "Maximum Value"
pMaxV <- pMaxV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsMeanV.rda")
pMeanV <- as.matrix(HRsMeanV)
pMeanV <- as.data.frame(pMeanV)
pMeanV$model <- "Mean"
pMeanV <- pMeanV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsMedianV.rda")
pMedianV <- as.matrix(HRsMedianV)
pMedianV <- as.data.frame(pMedianV)
pMedianV$model <- "Median"
pMedianV <- pMedianV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsMinV.rda")
pMinV <- as.matrix(HRsMinV)
pMinV <- as.data.frame(pMinV)
pMinV$model <- "Minimum Value"
pMinV <- pMinV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsRangeV.rda")
pRangeV <- as.matrix(HRsRangeV)
pRangeV <- as.data.frame(pRangeV)
pRangeV$model <- "Range"
pRangeV <- pRangeV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRVisV.rda")
pHRVisV <- as.matrix(HRVisV)
pHRVisV <- as.data.frame(pHRVisV)
pHRVisV$model <- "HRV index"
pHRVisV <- pHRVisV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/IRRRsV.rda")
pIRRRsV <- as.matrix(IRRRsV)
pIRRRsV <- as.data.frame(pIRRRsV)
pIRRRsV$model <- "IRRR"
pIRRRsV <- pIRRRsV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/MADRRsV.rda")
pMADRRsV <- as.matrix(MADRRsV)
pMADRRsV <- as.data.frame(pMADRRsV)
pMADRRsV$model <- "MAD-RR"
pMADRRsV <- pMADRRsV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/MeanNNsV.rda")
pMeanNNsV <- as.matrix(MeanNNsV)
pMeanNNsV <- as.data.frame(pMeanNNsV)
pMeanNNsV$model <- "Inter-beat Interval"
pMeanNNsV <- pMeanNNsV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/pNN50rV.rda")
ppNN50rV <- as.matrix(pNN50rV)
ppNN50rV <- as.data.frame(ppNN50rV)
ppNN50rV$model <- "pNN50"
ppNN50rV <- ppNN50rV[,c(3,4,293)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/rMSSDsV.rda")
prMSSDsV <- as.matrix(rMSSDsV)
prMSSDsV <- as.data.frame(prMSSDsV)
prMSSDsV$model <- "r-MSSD"
prMSSDsV <- prMSSDsV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/SDNNsV.rda")
pSDNNsV <- as.matrix(SDNNsV)
pSDNNsV <- as.data.frame(pSDNNsV)
pSDNNsV$model <- "SDNN"
pSDNNsV <- pSDNNsV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/SDSDsV.rda")
pSDSDsV <- as.matrix(SDSDsV)
pSDSDsV <- as.data.frame(pSDSDsV)
pSDSDsV$model <- "SDSD"
pSDSDsV <- pSDSDsV[,c(3,4,315)]
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/TINNsV.rda")
pTINNsV <- as.matrix(TINNsV)
pTINNsV <- as.data.frame(pTINNsV)
pTINNsV$model <- "TINN"
pTINNsV <- pTINNsV[,c(3,4,315)]

Valence <- NULL
Valence <- full_join(pAccV,pIQRV)
Valence <- full_join(Valence,pMADV)
Valence <- full_join(Valence,pMaxV)
Valence <- full_join(Valence,pMeanV)
Valence <- full_join(Valence,pMedianV)
Valence <- full_join(Valence,pRangeV)
Valence <- full_join(Valence,pMinV)
Valence <- full_join(Valence,pHRVisV)
Valence <- full_join(Valence,pIRRRsV)
Valence <- full_join(Valence,pMADRRsV)
Valence <- full_join(Valence,pMeanNNsV)
Valence <- full_join(Valence,ppNN50rV)
Valence <- full_join(Valence,prMSSDsV)
Valence <- full_join(Valence,pSDNNsV)
Valence <- full_join(Valence,pSDSDsV)
Valence <- full_join(Valence,pTINNsV)
colnames(Valence) <- c("Valence","Interaction","Model")

file = pMeanV
for (file in files){
  matrix <- as.matrix(pMeanV$bsp_moValence)
  ci <- posterior_interval(matrix)
  d <- data.frame(ci,file)
}

ValenceData$Model <- factor(ValenceData$Model,levels = c("TINN","SDSD","SDNN","r-MSSD", "pNN50", "MAD-RR", "IRRR", "Inter-beat Interval", "HRV index","Range","Minimum Value","Median","Mean Absolute Deviation","Mean","Maximum Value","Interquartile Range","Acceleration"))
  
HR <- c("Acceleration","Interquartile Range","Maximum Value","Mean","Mean Absolute Deviation","Median","Minimum Value","Range")
HR <- as.data.frame(HR)
ValenceData$FeatureGroup <- ifelse(ValenceData$Model %in% HR$HR,"HR","HRV")

write.csv(ValenceData,"Valence.csv")
```

# Univariate Valence Plot
```{r}
ValenceData <- read.csv("Valence.csv")

levels(ValenceData$Model)[levels(ValenceData$Model)=="Maximum Value"] <- "Maximum"
levels(ValenceData$Model)[levels(ValenceData$Model)== 'Minimum Value'] <- 'Minimum'
levels(ValenceData$Model)[levels(ValenceData$Model) == 'Interquartile Range'] <- 'IQR'
levels(ValenceData$Model)[levels(ValenceData$Model) == 'Mean Absolute Deviation'] <- 'MAD'
levels(ValenceData$Model)[levels(ValenceData$Model) == 'HRV index'] <- 'HRV Index'
levels(ValenceData$Model)[levels(ValenceData$Model) == 'Inter-beat Interval'] <- 'IBI'

ValenceData$Model <- factor(ValenceData$Model,levels = c("TINN","SDSD","SDNN","r-MSSD", "pNN50", "MAD-RR", "IRRR", "IBI", "HRV Index","Range","Minimum","Median","MAD","Mean","Maximum","IQR","Acceleration"))


valencePlot <- ggplot(ValenceData, aes(x = Valence, y = Model)) +
  geom_posterior(aes_string(x="Valence"),draw_sd=FALSE,draw_ci = TRUE,brighten=c(10, 1, 3),ci_width=0.95) + 
                 #fill="#587375",midline = "darkgrey") +
  ggtitle("Valence Coefficient") + 
  xlab("") + 
  ylab(" ") + 
  geom_vline(alpha=0.5, color="black", size=0.333, linetype=1, xintercept=0) +
  facet_grid("FeatureGroup ~ .", scales="free_y", space="free_y") +
    theme_minimal() +
   theme(
     legend.position = "none",
     #panel.spacing=unit(.05, "lines"),
     panel.border=element_rect(fill=NA, color="black", size=0.67),
     text=element_text(family="Times New Roman"), 
     strip.background = element_rect(color = "black", size = 0.67),
     axis.text=element_text(size=12),
     plot.title=element_text(size=16,hjust=0.5)
  )

valenceInterPlot <- ggplot(ValenceData, aes(x = Interaction, y = Model)) +
  geom_posterior(aes_string(x="Interaction"),draw_sd=FALSE,draw_ci = TRUE,brighten=c(10, 1, 3),ci_width=0.95) +
  ggtitle("Valence and Study") + 
  xlab("") + 
  ylab(" ") + 
  geom_vline(alpha=0.5, color="black", size=0.333, linetype=1, xintercept=0) +
  facet_grid("FeatureGroup ~ .", scales="free_y", space="free_y") +
    theme_minimal() +
   theme(
     legend.position = "none",
     #panel.spacing=unit(.05, "lines"),
     panel.border=element_rect(fill=NA, color="black", size=0.67),
     text=element_text(family="Times New Roman"), 
     strip.background = element_rect(color = "black", size = 0.67),
     axis.text=element_text(size=12),
     plot.title=element_text(size=16,hjust=0.5)
  )

## Close up at acceleration
ValenceAcc <- filter(ValenceData, Model == "Acceleration")

valencePlotAcc <- ggplot(ValenceAcc, aes(x = Valence, y = Model)) +
  stat_density_ci(center_stat = "mean", ci_width = 0.95,interval_type = "ci",) +
  geom_posterior(aes_string(x="Interaction"),draw_sd=FALSE,draw_ci = TRUE,brighten=c(10, 1, 3)) + 
                 #fill="#587375",midline = "darkgrey") +
  ggtitle("Valence Coefficient: Univariate, Acceleration") + 
  xlab("") + 
  ylab(" ") + 
  geom_vline(alpha=0.5, color="black", size=0.333, linetype=1, xintercept=0) +
  #facet_grid("FeatureGroup ~ .", scales="free_y", space="free_y") +
   theme_minimal() +
   theme(
     legend.position = "none",
     #panel.spacing=unit(.05, "lines"),
     panel.border=element_rect(fill=NA, color="black", size=0.67),
     text=element_text(family="Times New Roman"), 
     strip.background = element_rect(color = "black", size = 0.67),
     axis.text=element_text(size=12),
     plot.title=element_text(size=16,hjust=0.5)
  )

load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsAccV.rda")
pAccV <- as.matrix(HRsAccV)
pAccV <- as.data.frame(pAccV)
pAccV$model <- "Acceleration"
pAccV <- pAccV[,c(3,4,315)]

p <- plot(marginal_effects(HRsAccV, effects = "Valence:Study"))  # make pretty and put in appendix 

p$`Valence:Study` + 
  ggtitle("Acceleration Model: Interaction between Valence and Study") + 
  ylab("Heart Rate Acceleration") +
  theme_minimal() + 
     theme(
     text=element_text(family="Times New Roman"), 
     axis.text=element_text(size=12),
     plot.title=element_text(size=16,hjust=0.5)
  )
  
marginal_effects(HRsAccV,effect = "Valence:Study")

par(family = "Times")

plot(marginal_effects(HRsAccV,effect = "Valence:Study"),
     theme=theme_minimal())




ggplot(ValenceAcc, aes(x = Valence, y = Model)) +

```



#Combine plots
```{r}
library(ggpubr)

mainPlot <- ggarrange(arousalPlot,valencePlot)
annotate_figure(mainPlot,
                top = text_grob("Main Effect in Univariate Models",
                                size=20,
                                family = "Times New Roman"),
                                fig.lab.pos = "top")

# examine: 
# arousal: minimum value
# valence: maximum value and IBI 
# these three are rather wide compared to the rest -- when running models again, its the same result
interPlot <- ggarrange(arousalInterPlot,valenceInterPlot)
annotate_figure(interPlot,
                top = text_grob("Interaction Effect in Univariate Models",
                                size=20,
                                family = "Times New Roman"),
                                fig.lab.pos = "top")

#load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Valence/HRsAccV.rda")




#marginal_effects(HRsAccV,effect = "Valence:Study")
load("~/Dropbox/AU/5_semester/Ba/Analysis/Models/Arousal/HRsRangeA.rda")
marginal_effects(HRsRangeA,effect = "Arousal:Study")

library(bayesplot)

mcmc_areas(as.array(HRsAccV), 
   pars = c("bsp_moValence"),
   point_est = "mean")

```

# Rain plot - need posterior intervals (i.e. cis, maybe 90% to be "conservative")
```{r}
#Create theme
raincloud_theme <- theme(
  text = element_text(size = 10),
  axis.title.x = element_text(size = 16),
  axis.title.y = element_text(size = 16),
  axis.text = element_text(size = 14),
  axis.text.x = element_text(angle = 45, vjust = 0.5),
  legend.title = element_text(size = 16),
  legend.text = element_text(size = 16),
  legend.position = "right",
  plot.title = element_text(lineheight = .8, face = "bold", size = 16),
  panel.border = element_blank(),
  panel.grid.minor = element_blank(),
  panel.grid.major = element_blank(),
  axis.line.x = element_line(colour = "black", size = 0.5, linetype = "solid"),
  axis.line.y = element_line(colour = "black", size = 0.5, linetype = "solid"))

#Get flat violin
source("https://gist.githubusercontent.com/benmarwick/2a1bb0133ff568cbe28d/raw/fb53bd97121f7f9ce947837ef1a4c65a73bffb3f/geom_flat_violin.R")

coef(ValenceData$Valence)

lb <- function(x) mean(x) - sd(x)
ub <- function(x) mean(x) + sd(x)

sumld <- ValenceData %>% 
  select(-FeatureGroup) %>% 
  group_by(Model) %>% 
  summarise_all(funs(mean, median,confint(level=.9)))


ggplot(data = ValenceData, 
       aes(x = Model, y = Valence, fill = Model)) +
#  geom_flat_violin(position = position_nudge(x = .2, y = 0), alpha = .8) +
#  geom_point(aes(y = Valence, color = Model), 
#           position = position_jitter(width = .15), size = .5, alpha = 0.8) +
  geom_point(data = sumld, aes(x = Model, y = Valence_mean), 
             position = position_nudge(x = 0.3), size = 2.5) +
  geom_errorbar(data = sumld, aes(ymin = Valence_lower, ymax = Valence_upper, y = Valence_mean), 
                position = position_nudge(x = 0.3), width = 0) +
  expand_limits(x = 5.25) +
  coord_flip() + # flip or not
  theme_bw() +
  theme(legend.position="None")
```



